import parsing
import algo

GOUDRON = 0.66
SAND = 2
HERB = 1


def test_dijsktra_small_short():
    with open("./maps/small.data", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 7, 8)
        assert cost == GOUDRON


def test_dijsktra_small_long():
    with open("./maps/small.data", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 7, 16)
        assert cost == 3*GOUDRON


def test_dijsktra_campus_long():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 61, 942)
        assert round(cost, 2) == round(55*HERB + 7*GOUDRON, 2)


def test_dijsktra_campus_buzzer():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 61, 855)
        assert round(cost, 2) == round(35*HERB + 2*GOUDRON, 2)
