import itertools
import math

from utilz import *
from collections import deque, defaultdict
from heapq import *
from typing import Tuple

from pprint import pprint
import json


case_cost = defaultdict(lambda: 1)
case_cost["R"] = 0.66
case_cost["S"] = 2
case_cost["T"] = math.inf
case_cost["B"] = math.inf
case_cost["L"] = math.inf


def distance_to_tile(tile_info):
    case_type, travesable = tile_info
    if case_type in "0123456789":
        return 1.
    if not travesable:
        return math.inf
    return case_cost[case_type]


def distance(state: State, start: int, goal: int, **kwargs):
    return dijkstra(state, start, goal, **kwargs)[0]


def dijkstra(state: State, start: int, goal: int, avoidGuard = True) -> Tuple[int, dict]:
    """
    Get distance from current positon of player and goal
    :return (cost in turns) + dict(nodes)
    """
    stack = [(0, start)]
    heapify(stack)
    previous = {start: None}

    # pprint(("dijkstra", start, goal))
    while stack:
        dist, node = heappop(stack)

        if node == goal:
            history = [node]
            while previous[node] is not None:
                node = previous[node]
                history.append(node)
            return dist, history[::-1]

        # pprint(("popped", dist, node))
        for _, (neighbor, tile_info) in get_neighbours(node, state).items():

            if neighbor in previous:
                continue

            cost = distance_to_tile(tile_info) + dist
            if avoidGuard and node == start and OpponentInfo('G', neighbor) in state.visibleOpponents:
                cost = math.inf

            if math.isinf(cost):
                continue

            # pprint(("set neighbor", neighbor, cost, tile_info))
            heappush(stack, (cost, neighbor))
            previous[neighbor] = node

    
    # with open("debug_dijkstra.txt", "w+") as f:
    #     print(json.dumps(list(previous.keys())), file=f)
    return -1, []


class dijkstraCache:
    def __init__(self, state):
        self.state = state
        self.cache = {}

    def get_distance(self, start: int, goal: int):
        if (start, goal) in self.cache:
            return self.cache[(start,goal)]
        dist = distance(self.state, start, goal, avoidGuard=False)
        self.cache[(start, goal)] = dist
        return dist


def precompute_buzzers_distances(state):
    dijkstra_cache = dijkstraCache(state)
    buzzers = useful_targets_ids(state)[1:]
    for (buzz_a, buzz_b) in itertools.permutations((0, 1, 2, 3, 4), 2):
        dijkstra_cache.get_distance(buzzers[buzz_a], buzzers[buzz_b])
    return dijkstra_cache


def calc_initial_paths_dists(state: State, dijsktra_cache: dijkstraCache = None):
    """
    Ordre des targets : me, i, m, e, a, c
    """

    if dijsktra_cache is None:
        dijsktra_cache = dijkstraCache(state)

    useful_targets = useful_targets_ids(state)
    paths = []

    for path in itertools.permutations((1, 2, 3, 4, 5)):
        # betonnière first
        if not(path[0]) == 5:
            continue
        prev = 0
        dst = 0
        for buzzid in path:
            dst += dijsktra_cache.get_distance(useful_targets[prev], useful_targets[buzzid])
            # print(prev, buzzid, dst)
            prev = buzzid

        paths.append((dst, [useful_targets[p] for p in path]))

    paths.sort()
    return paths[0]
