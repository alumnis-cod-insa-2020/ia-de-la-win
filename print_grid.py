import sys

def print_grid(state):
    grid, w, h = state.grid, state.colCount, state.lineCount
    with open("grid_data.txt", 'w') as f:
        for j in range(h):
            for i in range(w):
                index = w*j+i
                f.write(" "*(j%2==1) + " " + str("{:2d}".format(index) ) + grid[index].type, end=",")
            f.write()
