from algo import dijkstra

class dijsktraCache:
    def __init__(self, state):
        self.state = state
        self.cache = {}

    def compute(start: int, goal: ind):
        if (start, end) in self.cache:
            return self.cache[(start,end)]
        return dijstra(self.state, start, goal)

