from collections import namedtuple
from dataclasses import dataclass
from typing import Dict, Tuple
import logging

"""
W : mur en brique
R : goudron
G : herbe
T : arbre
S : sable (progression lente)
L : limite de terrain (gros mur)
0 : porte 2 GEI
1 : porte 1 GEI
2 : porte 1 GM
3 : porte 2 GM
4 : porte 1 GMM
5 : porte 2 GMM
6 : porte 1 GC
7 : porte 2 GC
8 : porte 1 GPE
9 : porte 2 GPE
E : sol GPE
M : sol GM
C : sol GC
A : sol GMM
I : sol GEI
B : béton (posé par un super-pouvoir)
e : buzzer GPE
m : buzzer GM
c : buzzer GC
a : buzzer GMM
i : buzzer GEI
"""

POWER_NONE = 0
POWER_DOOR = 1
POWER_CROWBAR = 2
POWER_VISION = 3
POWER_INVISIBILITY = 4
POWER_WALL = 5

TileInfo = namedtuple("TileInfo", "type traversable")
OpponentInfo = namedtuple("OpponentInfo", "id pos")


class State:
    def __init__(self,
                 grid: dict,
                 colCount: int,
                 lineCount: int,
                 ourPos: int = -1,
                 ourPower: int = 0,
                 ourSuspectedStatus: int = 0,
                 visibleOpponents: list = None):

        if not visibleOpponents:
            self.visibleOpponents = []
        else:
            self.visibleOpponents = visibleOpponents

        self.grid = grid
        self.colCount = colCount
        self.lineCount = lineCount
        self.ourPos = ourPos
        self.ourPower = ourPower
        self.ourSuspectedStatus = ourSuspectedStatus

    def __repr__(self):
        return f"State(ourPos={self.ourPos}, ourPower={self.ourPower}, ourSuspectedStatus={self.ourSuspectedStatus}, visibleOpponents={self.visibleOpponents})"

    def __str__(self):
        return repr(self)


def get_accessible_neighbours(pos: int, state: State):
    return {k: v for k, v in get_neighbours(pos, state).items() if v[1].traversable and not v[0]=="B"}


def get_neighbours(pos: int, state: State) -> Dict[int, Tuple[int, TileInfo]]:
    return {k: (v, state.grid[v]) for k, v in get_neighbours_idxes(pos, state).items()}


def get_abs_idx(i, j, state):
    return i * state.colCount + j


def get_neighbours_idxes(pos: int, state: State):
    i, j = divmod(pos, state.colCount)
    if i%2:
        return {
            1: get_abs_idx(i - 1, j + 1, state),
            2: get_abs_idx(i, j + 1, state),
            3: get_abs_idx(i + 1, j + 1, state),
            4: get_abs_idx(i + 1, j, state),
            5: get_abs_idx(i, j - 1, state),
            6: get_abs_idx(i - 1, j, state),
        }
    else:
        return {
            1: get_abs_idx(i - 1, j, state),
            2: get_abs_idx(i, j + 1, state),
            3: get_abs_idx(i + 1, j, state),
            4: get_abs_idx(i + 1, j - 1, state),
            5: get_abs_idx(i, j - 1, state),
            6: get_abs_idx(i - 1, j - 1, state),
        }
    

def useful_targets_ids(state):
    tgt = [state.ourPos]
    for v in "imeac":
        for i in range(state.colCount * state.lineCount):
            if state.grid[i].type == v:
                tgt.append(i)
    return tgt


def do_i_run(track, state, stamina):
    if not track:
        return False

    if len(track) < 4:
        case_type, travesable = state.grid[track[0]]

        if case_type == "R" and stamina > 0:
            return True
        else:
            return False

    case_type1, travesable = state.grid[track[0]]

    if stamina == 2 and case_type1 == "R":
        return True

    case_type2, travesable = state.grid[track[1]]
    case_type3, travesable = state.grid[track[2]]

    if case_type1 == "R" and case_type3 != "R" and case_type2 != "R" and stamina > 0:
        return True

    if case_type1 == "R" and case_type3 == "R" and case_type2 != "R":
        return True

    return False


def do_i_use_my_crowbar(s, next_move):
    return s.ourPower == POWER_CROWBAR and not s.grid[next_move][1]
