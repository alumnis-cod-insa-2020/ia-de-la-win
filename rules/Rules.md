# Présentation du jeu

Comme tout étudiant lambda de l'INSA, vous avez une vie nocturne très riche, et, tellement amoureux de votre campus, vous aimez vous y balader la nuit ! Votre objectif ultime est d'activer tous les buzzers présents dans les départements (au nombre de 5). Bien évidemment il vous est strictement interdit d'être dans les bâtiments de nuit et plus généralement de pratiquer toute activité suspecte sur le campus de nuit. Les vigiles sont là pour vous le rappeler et vous emmèneront à la loge pour une explication devant l'équipe de sécurité s'ils vous attrapent (en gros vous perdez) !


# Modèle

## Paramétrage de la grille

Bon nous sommes d'accord, les grilles cartésiennes sont trop faciles, et comme à Toulouse on aime faire compliqué, le jeu se déroulera sur une grille hexagonale (*petite pensée à ceux qui étaient là il y a 2 ans :)*).
Il existe de nombreuses manières de modéliser une grille hexagonale et nous avons opté pour la manière la plus intuitive qui est basée sur un changement de variable à partir d'une grille cartésienne.
Pour plus d'informations sur les modélisations possibles d'une grille hexagonale, se référer à [ce lien](https://www.redblobgames.com/grids/hexagons/).</br>
La grille peut être modélisée par une matrice dont les lignes sont décalées à droite pour les lignes impaires. <span style="text-decoration:underline">Nous vous recommandons d'utiliser cette modélisation.</span>

![Mesh](mesh.png)
<!--
    0   1   2   3   4   5 = (Nx-1)
0 | 0   1   2   3   4   5
1 |   6   7   8   9   10  11
2 | 12  13  14  15  16  17
3 |   18  ..................
..
Ny-1
-->

La matrice a Nx.Ny valeurs.
L'indice d'une cellule de coordonnées (i,j) est j*Nx+i
Chaque cellule possède une indexation locale de ses cellules adjacentes.
Les commandes pour bouger d'une cellule à une autre :
![Localindexing](local_indexing.png)
<!--
      6   1
       / \
     5 | | 2
       \ /
      4   3
-->
| Cellule adjacente | Indice local |
| --- | --- |
| Au dessus à droite | 1 |
| A droite | 2 |
| En dessous à droite | 3 |
| En dessous à gauche | 4 |
| A gauche | 5 |
| Au dessus à gauche | 6 |
Cette indexation est utilisée pour communiquer les déplacements de cellule en cellule.

## Déplacement
A chaque tour le joueur peut se déplacer sur les cellules adjacentes à sa cellule courante.</br>
Deux modes de déplacement existent :
- marche normale : déplacement cellule par cellule adjacentes
- marche fast : déplacement par lot de deux cellules (le joueur devra alors préciser la cellule intermédiaire de déplacement et la cellule finale).

Si un vigile a le joueur dans son champ de vision, ce mode de déplacement le rend automatiquement suspect.</br>
Il est possible de courir seulement sur du goudron. Si le joueur tente de courir sur une cellule où il ne peut pas, ce sera seulement l'identifiant de la première cellule (cellule intermédiaire) qui sera pris en compte. Plus précisément, la condition vérifiée pour courir est que la cellule intermédiaire soit du goudron, un mouvement du type herbe-goudron-herbe est donc accepté.</br>
De plus, lorsqu'un joueur marche dans du sable, à chaque tour il a une probabilité 0.7 d'arriver à bouger et 0.3 de rester sur la cellule courante.</br>
Un joueur ne peut courir plus de 2 tours d'affilée à cause de l'essoufflement (*oui parce que bon on a beau dire qu'à l'INSA on fait du sport, on mange tout le temps des pizzas...*). Au bout des 2 tours, il ne pourra pas courir pendant 2 tours (possibilité par contre de faire par exemple 1 tour en courant puis 1 tour en marchant, etc.).
Les vigiles, eux, se déplacent cellule par cellule, en marchant.

## Suspicion

Les vigiles ne sont pas là pour faire copain-copain avec vous ! Votre objectif est de leur échapper le plus longtemps possible mais ceux-ci feront tout pour vous arrêter.</br>
Les vigiles effectuent régulièrement des rondes sur le campus de l'INSA.</br>
Si un vigile vous voit dans l'une des configurations suivantes vous êtes suspectés :
- courir sur le campus
- disposer d'un super-pouvoir
- être dans un bâtiment (être sur une cellule porte ne compte pas comme être dans le bâtiment)

Si le vigile découvre qu'une porte est cassée il prendra le premier joueur qu'il croise pour suspect.</br>
Une fois que vous êtes suspectés le vigile se met à vos trousses (*possiblement avec des chiens, faîtes attention à protéger vos chats de Cod'INSA !*). Vous êtes suspectés tant que vous êtes dans son champ de vision. <!--[et pendant 5 tours après que vous en ayiez disparu] -> pas sur de le mettre en place.-->

## Interaction avec les autres utilisateurs et ordre de jeu

Il n'y a pas d'exclusion sur la présence sur une case : les joueurs comme les vigiles peuvent occuper une même case en même temps.
En revanche si plusieurs joueurs occupent une même case et que l'un d'entre eux est suspecté, un joueur innocent peut devenir suspecté avec une probabilité 0.5. Le joueur suspecté peut alors devenir innocent.

Des étudiants ivres parcourent aussi la carte pendant la partie. Ils ne sont que spectateurs et n'ont pas d'interaction avec le gens. Ivres, ils n'ont pas de trajectoire bien déterminée (mouvement brownien) mais ils ne rentrent pas dans les bâtiments. Un joueur ne peut pas occuper une cellule occupée par un étudiant ivre.

A chaque tour chacun des participants joue (en même temps), puis c'est au tour des vigiles et des étudiants ivres de jouer. Les données qui sont communiquées aux joueurs pour qu'ils réalisent leurs actions du tour N+1 correspondent donc à l'état de la partie à l'état N.

Un vigile vous attrape s'il est sur la même cellule que vous. Le test est réalisé précisemment juste avant que vous jouiez.

## Champ de vision

<!-- Votre champ de vision a par défaut une portée de 4 cellules. Les vigiles eux voient à 5 cellules de portée en extérieur.</br> -->
Votre champ de vision a par défaut une portée de 4 cellules alors que les vigiles eux voient à 5 cellules.</br>
<!-- Quand un vigile est à l'intérieur d'un bâtiment, il voit en plus des cellules qu'il voit par défaut en extérieur, l'intégralité des cellules de l'intérieur. -->

## Bâtiments

Chaque bâtiment comporte 2 portes. Ces portes sont par défaut dans l'état ouvert sauf si une action spécifique a été faite dessus (cf ci-après).</br>
L'objectif du jeu étant d'activer tous les buzzers des bâtiments, vous allez devoir vous y introduire.
Lorsqu'un joueur active un buzzer dans un bâtiment, il récupère automatiquement le super-pouvoir de ce département (cf section suivante).</br>
Lorsqu'un joueur se place sur une porte d'un bâtiment, il peut savoir si quelqu'un est dans le bâtiment (sans distinction de si c'est un vigile ou un autre joueur).

## Actions spéciales

Des actions spéciales (super-pouvoirs) sont à disposition dans les bâtiments.

| Bâtiment | Action |
| --- | --- |
| GEI (*Génie Electronique et Informatique*) | Peut piloter les portes à distance |
| GM (*Génie Mécanique*) | Donne le pied de biche pour ouvrir les portes verrouillées |
| GMM (*Génie Mathématique et Modélisation*) | Améliore la visibilité grâce à des lunettes deep-learning (portée de 7 cellules en visibilité) |
| GPE (*Génie des Procédés et Environnement*) | Potion magique pour être invisible 10 tours pendant la partie |
| GC (*Génie Civil*) | Construire un mur |

Un joueur ne peut avoir qu'un seul super-pouvoir à la fois. S'il en prend un alors qu'il en a déjà un autre, le nouveau super-pouvoir prend automatiquement le dessus.</br>
Les supers-pouvoirs se récupèrent dans les pièces des bâtiments, au niveau des buzzers. Une fois muni d'un super-pouvoir, le joueur peut l'utiliser à n'importe quel endroit sur la carte.</br>
Un joueur peut se débarrasser d'un super-pouvoir, il n'a alors plus rien.</br>
Les pouvoirs du GMM (augmenter la visibilité), du GPE (potion magique) et du GC (mettre du béton) ne peuvent être utilisés qu'une seule fois quand le joueur a un des pouvoirs a sa disposition. En revanche, il peut de nouveau acquérir le pouvoir par la suite alors qu'il l'a utilisé en revenant sur le buzzer associé (même si celui-ci a déjà été activé).
A contrario, les pouvoirs du GM et du GEI peuvent être utilisés plusieurs fois.
Le pouvoirs du GMM et du GPE, une fois activés fonctionnent respectivement pendant 7 et 10 tours.

Lorsqu'un vigile passe par une porte verrouillée, la porte se déverrouille automatiquement.</br>
Même si un joueur dispose du super-pouvoir de verrouiller les portes, afin de pouvoir en franchir une le joueur est obligé de déverrouiller (et donc déverrouiller pour tous les autres joueurs).

Représentation des super-pouvoirs :
| ID du super-pouvoir | Super-pouvoir | Bâtiment associé |
| --- | --- | --- |
| 0 | aucun | aucun |
| 1 | pilotage des portes à distance | GEI |
| 2 | pied de biche | GM |
| 3 | lunettes deep-learning | GMM |
| 4 | potion magique | GPE |
| 5 | construire un mur | GC |

Enfin, à un tour donné, si un joueur décide de réaliser une action et de se déplacer en même temps, l'action est réalisée sur la cellule de départ, c'est-à-dire avant le déplacement.

## Représentation de la carte dans le fichier texte

- W : mur en brique
- R : goudron
- G : herbe
- T : arbre
- S : sable (progression lente)
- L : limite de terrain (gros mur)
- 0 : porte 2 GEI
- 1 : porte 1 GEI
- 2 : porte 1 GM
- 3 : porte 2 GM
- 4 : porte 1 GMM
- 5 : porte 2 GMM
- 6 : porte 1 GC
- 7 : porte 2 GC
- 8 : porte 1 GPE
- 9 : porte 2 GPE
- E : sol GPE
- M : sol GM
- C : sol GC
- A : sol GMM
- I : sol GEI
- B : béton (posé par un super-pouvoir)
- e : buzzer GPE
- m : buzzer GM
- c : buzzer GC
- a : buzzer GMM
- i : buzzer GEI


## Temps de réponse

Le temps donné à chaque IA pour répondre est limité.
Une fois que les données d'initialisation vous ont été envoyées vous disposez de 10 secondes pour renvoyer le statut `ok` qui indique la fin de vos prétraitements.
Si un joueur ne répond pas dans les temps à l'initialisation il est exclu de la partie (son avatar sera présent sur la carte mais ne pourra réaliser aucune action).</br>

A chaque tour vous disposez de 1.1 seconde pour répondre (le dixième, c'est pour prendre en compte le ping, sujet à légère augmentation en cas de nécessité)
Si ce délai n'est pas respecté au tour N le serveur *passera votre tour*. Cela signifie que : 
- Vous ne récupérerez pas les données du tour suivant ;
- Toute information que vous envoyez sera traitée au tour suivant

*Pour ceux qui ne comprennent pas à quel point cette partie est critique, voici un exemple :*</br>
Au tour N, vous ne respectez pas le délai de temps qui vous a été accordé. Aucune donnée ne sera traitée pour l'IA au tour N et les données du tour N+1 ne seront pas envoyées. Si vous répondez dans les temps au tour N+1, alors votre réponse sera jouée au tour N+1.
Si vous ne répondez toujours pas dans les temps au tour N+1, le serveur passera de nouveau votre tour et vous n'aurez pas les données du tour N+2 et si vous répondez dans les temps au tour N+2, alors les données que vous envoyez seront traitées à ce tour.</br>
*Bref, vous avez compris ? Soyez ponctuels !!*

## Conditions de victoire et classement

Le joueur qui active en premier l'intégralité des buzzers gagne la partie.</br>
Une fois qu'un joueur a activé tous les buzzers, la partie continue jusqu'à ce que tous les joueurs aient activé les buzzers ou soient morts. Un classement est établi à partir de l'ordre d'activation de tous les buzzers entre les participants.
Chaque partie réalise au maximum 500 tours. Si aucun des joueurs n'a activé tous les buzzers, c'est le premier joueur à avoir activé le maximum de buzzers qui remporte la partie</br>
Le niveau de difficulté des parties est déterminé par le nombre de vigiles. A chaque niveau de difficulté, 5 matchs sont joués et les classements sont moyennés. Au bout des 5 matchs, le joueur le plus mal classé est disqualifié et les joueurs restants jouent de nouveau 5 matchs avec un vigile supplémentaire.</br>

La carte qui vous est fournie pour le développement est une légère variante de la carte qui sera utilisée pour les matchs de la finale. Certains éléments tels que la position des portes, la nature de certaines cellules (herbe, goudron, sable) peuvent varier.


# Communication

## Données d'entrée

Au début de chaque partie l'intégralité de la carte est communiquée selon le format suivant :

```
INIT
<nombre entier d'éléments sur une ligne> <nombre entier d'éléments sur une colonne>
W W W W W W
 W R R R R W
W R R R R W
 W W W W W W
N <id>
<nom joueur 2> <id joueur 2>
<nom joueur 3> <id joueur 3>
```

Toutes les lignes ont le même nombre de cellules.</br>
Les lignes d'indice pair (l'indexation commençant à 0) ne sont pas décalées alors que les lignes d'indices impairs sont décalées à droite.</br>
L'indexation se fait selon la norme indiquée plus haut.

N correspond au nombre total de joueurs (vous y-compris).</br>
Cette ligne est suivie de N-1 lignes donnant les noms de chacun des joueurs.</br>
`<id>` correspond à l'identifiant (entier) qui vous a été attribué</br>

Il se peut que des erreurs soient produites par le serveur à l'initialisation (mauvais token, erreur de communication, etc.) Dans ce cas là la première ligne ne contient par le mot clé 'INIT' mais l'erreur renvoyée.

N'oubliez pas, une fois que vous avez terminé de traiter les données d'initialisation et que vous êtes prêts pour commencer la partie, de renvoyer le statut `ok`.



A chaque tour des informations sont données sur le joueur et sur les cases alentours.

```
TURN <Ntour>
I T
P
S
Q <1 ou 0> (ligne optionnelle)
N
33 W 0
34 G 1
...
<N lignes>
M
0 35
G 33
<M lignes>
EOT
```


avec TURN une ligne précisant l'état actuel du jour (voir les autres alternatives ci-après), le chiffre suit ce mot clé correspond au numéro du tour</br>
I l'indice de la cellule courante</br>
T le type</br>
P le pouvoir qu'a le joueur actuel (représenté par un entier, 0 si aucun)</br>
S un entier disant si le joueur est suspecté (0 pour innocent, 1 si suspecté et 2 si le joueur vient de se faire attraper)</br>
Si le joueur se trouve sur une cellule correspondant à une porte, une ligne commençant par Q puis suivie d'un 1 ou d'un 0 indique si quelqu'un (vigile ou autre joueur) se trouve dans le bâtiment (pas de distinction si un ou plusieurs personnes dans le bâtiment). Cette ligne n'est pas présente si le joueur ne se trouve pas sur une porte.</br>
N le nombre de cellule que le joueur peut voir (valeur variable en fonction de ses pouvoirs, de la position sur la carte et des parties)</br>
Les N lignes suivantes décrivent l'environnement : `<id cellule> <type> <parcourable>`</br>
Le 3ème élément de chaque ligne sert à savoir si un joueur peut parcourir une cellule. `<parcourable>=0 ou 1`</br>
Cela inclut les cellules telles que les murs mais aussi les cellules dont l'état peut varier. En effet, les portes des bâtiments ne peuvent être parcourues si elles sont verrouillées. De même, le Génie Civil (GC) donne le pouvoir de mettre du béton sur le sol rendant les cellules affectées non parcourables. Si un étudiant ivre est sur une cellule un joueur ne pourra pas s'y rendre, c'est indiqué via le statut parcourable</br>
M le nombre d'adversaire (autres joueurs ou vigiles) que le joueur voit dans son champ de vision</br>
Les M lignes suivantes donnent l'identifiant d'un éventuel joueur adverse ou gardien ou étudiant ivre et l'identifiant de la cellule sur laquelle il se situe.</br>
Les gardiens ont tous pour identifiant `G`.</br>
Les étudiants ivres ont tous pour identifiant `D`.</br>
EOT indique la fin des données pour le tour courant

Si le joueur vient de gagner ou de perdre, le message envoyé contient simplement `WON` ou `KILLED` sans rien d'autre.</br>
Si le nombre maximal de tours vient d'être atteint, le serveur renvoie simplement `END`.</br>
Dans ces 3 cas il n'y a pas besoin de renvoyer de message et le programme du joueur peut se fermer.</br>


## Données en sortie

Au tout début de la partie, après vous être connectés au serveur ZMQ vous devez fournir un token. Ce token doit être connu de vous uniquement (sinon les autres équipes peuvent se connecter à votre place pendant les matchs).</br>
La ligne doit être de la forme :
`token <votre token>`

Le joueur renvoie une liste d'actions à effectuer. A chaque tour il ne peut y avoir qu'une seule action parmi les suivantes :
- se déplacer : `M <direction de déplacement>={1,2,3,4,5,6}`
- se déplacer en courant : `MF <direction de déplacement 1> <direction de déplacement 2>`

Le joueur peut aussi communiquer, en plus d'une des actions précédentes, les actions suivantes en les donnant sur des lignes différentes.
- utiliser un super-pouvoir : P
- se débarrasser du super-pouvoir : S

L'utilisation d'un super-pouvoir se fait comme suit :
- GEI : `P 0` pour déverrouiller et `P 1` pour verrouiller
- GM : `P` pour casser une porte (ne marche que si le joueur est adjacent à une porte)
- GMM : `P` pour activer les lunettes
- GC : `P` pour mettre du béton sur la cellule courante (si le joueur ne bouge pas il est alors dans le béton, il peut y rester autant de temps qu'il veut, s'il bouge de cette cellule, il ne pourra plus revenir dessus)
- GPE : `P`

Les instructions renvoyées doivent impérativement contenir une dernière ligne `EOI` pour indiquer la fin des instructions pour ce tour.
Si aucune ligne contenant EOI n'est renvoyée avant la fin du tour, le tour est passé par le joueur et aucune instruction n'est prise en compte (même si du contenu a été renvoyé avant).


## Serveurs de développement

### Serveurs par équipe

Chaque équipe dispose de 6 instances du serveur afin que vous puissiez tester vos IA. Ces instances vous permettent d'effectuer des tests individuels uniquement, sans équipe adverse : http://deepdev.ddns.net:6001/serverIndiv</br>
Chaque équipe dispose aussi d'une instance de test permettant d'effectuer des batailles dans la même équipe, si vous souhaitez confronter différentes IA par exemple : deepdev.ddns.net:6002/serverBattleTeam</br>
Pendant la phase de développement vous pouvez configurer ces instances du serveur en choisissant le nombre de vigiles, le délai de réponse, etc.
Ceux-ci sont accessibles via une page web. Sur cette page vous devrez renseigner le token qui vous est attribué.</br>
Pendant une partie vous avez accès aux messages donnés par le serveur. Ces informations sont données en temps réel via l'interface web à titre informatif pour savoir quelles sont les informations que le serveur reçoit, ce qu'il comprend et ce qu'il fait.</br>
Pendant chaque partie un log dont le format est cette fois-ci codifié est aussi généré. L'intégralité des fichiers log est conservé et vous pouvez y accéder à tout moment. Ces fichiers ne retranscrivent que ce que le serveur réalise en temps qu'actions sur le plan du jeu. Par exemple si vous essayez de courir alors que votre joueur est épuisé, ce log ne vous indiquera qu'un seul déplacement sans autres informations alors que les informations plus riches données par le serveur vous indiqueront que vous avez essayé de courir mais qu'il n'a pas pris en compte cette action.</br>
Par exemple un fichier log peut ressembler à:
```
Toulouse0 0 
NGUARD 1
TOUR 0
PLAYER 0 MOVETO 76 27
GUARD 0 MOVETO 4 4
STUDENT 0 MOVETO 66 12
STUDENT 1 MOVETO 16 6
STUDENT 2 MOVETO 47 6
STUDENT 3 MOVETO 72 5
STUDENT 4 MOVETO 5 29
STUDENT 5 MOVETO 29 17
STUDENT 6 MOVETO 78 20
TOUR 1
PLAYER 0 MOVETO 75 27
GUARD 0 MOVETO 4 5
STUDENT 0 MOVETO 65 13
STUDENT 1 MOVETO 17 6
STUDENT 2 MOVETO 48 6
STUDENT 3 MOVETO 72 6
STUDENT 5 MOVETO 29 18
STUDENT 6 MOVETO 78 21
```

### Serveur commun

Enfin, une dernière instance du serveur fonctionne en permanence et est commune à toutes les équipes. Des combats sont joués automatiquement toutes les 90 secondes. Vous pouvez donc choisir à tout moment du développement de vos IA de les confronter à celles des équipes adverses en rejoignant ce serveur de jeu : http://deepdev.ddns.net:6003/battleManager

### Pour la finale

Pour les combats finaux en tant que tels, ceux-ci seront lancés sur un ordinateur de l'équipe organisatrice. Toutes les instances des serveurs seront alors inaccessibles en dehors du réseau local. A partir de 10h le dimanche, des identifiants vous seront fournis pour vous connecter en SSH à votre session et y installer votre IA. C'est à vous de vous assurer que toutes les librairies sont installées. S'il y a besoin d'installer des librairies particulières merci de contacter l'équipe technique.
Un script devra être placé dans le répertoire *Documents* de votre home directory s'intitulant *launcher.sh* (*~/Documents/launcher.sh*). C'est ce script qui sera exécuté pour le lancement de vos IA.


### Statistiques

Si vous avez lu jusqu'ici les règles du jeu c'est que vous avez fait preuve de courage (ou qu'il est 3 heures du matin et que vous êtes désespérés sur le sujet). Bonne nouvelle pour vous récompenser de vos efforts, malgré le sadisme profond dont a fait preuve votre superbe équipe organisatrice avec ce sujet, nous allons vous récompenser d'avoir lu jusqu'ici ! Afin de vous rassurer et de vous permettre de réaliser que toutes les autres équipes sont dans la même ***** que vous, vous pouvez visualiser les statistiques de lancement des serveurs de jeu individuels de toutes les équipes ici : http://deepdev.ddns.net:6006/statsWebpage



# Exemples pour commencer

Pour pouvoir commencer facilement quelques exemples d'utilisation de ZMQ dans différents langages sont proposés.
