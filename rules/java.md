# Installation
```bash
sudo apt-get install libtool autoconf automake uuid-dev e2fsprogs
git clone git://github.com/zeromq/libzmq.git
./autogen.sh
./configure
make
sudo make install
ldconfig -v
echo $JAVA_HOME
export JAVA_HOME=/location/to/your/java/installation
```

**Example :**
export JAVA_HOME=/usr/lib/jvm/jdk1.7.0_17

```bash
git clone https://github.com/zeromq/jzmq.git
./autogen.sh
./configure
make
sudo make install
```

# Java project
It should be a Maven Project. For the Maven artifact ID and group ID, write what you want.

## In pom.xml
```xml
<!-- https://mvnrepository.com/artifact/org.zeromq/jeromq -->
<dependency>
    <groupId>org.zeromq</groupId>
    <artifactId>jeromq</artifactId>
    <version>0.5.2</version>
</dependency>
```


## Minimal code

```java


import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zeromq.*;


public class Main {
	
	// Fonction d'écriture
	public static void write(ZMQ.Socket socket, String content){
		String newString;
		try {
			newString = new String(content.getBytes("UTF-8"), "UTF-8");
			socket.send(newString);
		} catch (UnsupportedEncodingException e) {
			System.out.println("ERROR : Cannot write content");
			e.printStackTrace();
		}
		
	}
	
	// Fonction de lecture
	public static String read(ZMQ.Socket socket){
		byte[] reply = socket.recv(0);
		return new String(reply, ZMQ.CHARSET);
	}

	
	 public static void main(String[] args) throws Exception {
		 // Configuration
		 String token = "gagner";
		 String IP = "192.168.1.10";
		 int port = 5555;
         
         try (ZContext context = new ZContext()) {
 			System.out.println("Bienvenue sur le serveur de Cod'INSA en mode chocolatine !");
            
 			// Connection au serveur
			ZMQ.Socket socket = context.createSocket(SocketType.REQ);
            socket.connect("tcp://" + IP + ":"+Integer.toString(port));
            
            // Envoi du token
            write(socket, "token " + token);
            
            // On récupère toutes les données
            String raw_data = read(socket);
            
            // Puis on les traite
            String[] data_lines = raw_data.split("\n");
            
            int Nx,Ny ;
            String [] N = data_lines[0].split(" ");
            Nx = Integer.parseInt(N[0]);
            Ny = Integer.parseInt(N[1]);
            
            // Replissage de la map
            List<String[]> map = new ArrayList<String[]>();
            
            for (int j = 0; j < Ny; j++){
            	map.add(data_lines[j+1].split(" "));
            }
            
            int nbPlayer, myId;
            String [] players = data_lines[Ny+1].split(" ");
            nbPlayer = Integer.parseInt(players[0]);
            myId = Integer.parseInt(players[1]);
            
            // La liste des adversaires
            Map<Integer, String> list_opponents = new HashMap<Integer, String>();
            for (int c = Ny+2; c < data_lines.length; c++){
            	String[] infoIA = data_lines[c].split(" ");
            	String name = infoIA[0];
            	int idIA = Integer.parseInt(infoIA[1]);
            	list_opponents.put(idIA, name);
            }
            
            // Enfin on dit qu'on est prêt
            write(socket, "ok");
         }

	}
}

```
