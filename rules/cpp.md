# Avec Linux...

## Installation

```sh
# Build, check, and install libsodium
git clone http://github.com/jedisct1/libsodium.git
cd libsodium
./autogen.sh
./configure && make check
sudo make install
sudo ldconfig
cd ../
# Build, check, and install the latest version of ZeroMQ
git clone http://github.com/zeromq/libzmq.git
cd libzmq
./autogen.sh
./configure --with-libsodium && make
sudo make install
sudo ldconfig
cd ../
# Now install cppzmq
git clone https://github.com/zeromq/cppzmq.git
cd cppzmq
mkdir build
cd build
cmake ..
sudo make -j4 install
```

## CMakeLists

```
cmake_minimum_required(VERSION 3.0)

project(codeinsa)
set(CMAKE_CXX_STANDARD 11)

find_package(cppzmq)

add_executable(a src/main.cpp)
target_link_libraries(a cppzmq)
```

# Avec Windows...

## Prérequis 

Pour pouvoir lancer votre IA, il vous faudra utiliser ZMQ. Pour cela, il va falloir compiler ZMQ pour Windows en utilisant CMake. Attention, ZMQ ne semble pas supporté par Windows 10 pour le moment (janvier 2020) au vu des instructions sur github !
- CMake : https://cmake.org/
- Les sources de ZMQ : https://github.com/zeromq/libzmq
- Les sources de CppZMQ : https://github.com/zeromq/cppzmq

## Compiler ZMQ

En utilisant CMAKE, vous devriez pouvoir compiler zmq sans soucis dans un terminal :
```
mkdir build
cmake ..
```

Il vous faudra ensuite compiler la librairie avec votre compilateur préféré suite à la génération réalisée avec CMAKE

_Faire plus de tests_

# Minimal code

```cpp
#include <iostream>
#include <zmq.hpp>

void write(zmq::socket_t& socket, const std::string& str)
{
  zmq::message_t msg(str.c_str(), str.size());
  socket.send(msg, zmq::send_flags::dontwait);
}

std::string read(zmq::socket_t& socket)
{
  zmq::message_t msg;
  socket.recv(msg, zmq::recv_flags::none);
  return std::string((const char *)msg.data(), msg.size());
}

int main()
{
    std::string initRequest = "token [[myTokenHere]]";
    std::string ip = "192.168.217.138";
    std::string port = "5555";

    zmq::context_t ctx;
    zmq::socket_t sock(ctx, zmq::socket_type::req);
    sock.connect("tcp://" + ip + ":" + port);
    write(sock, initRequest);
    std::string response = read(sock);
    std::cout << response << std::endl;

    return 0;
}
```
