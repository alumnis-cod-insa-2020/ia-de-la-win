
## Python

Pour commencer à développer il vous faut une implémentation de ZMQ pour Python. Pour cela vous pouvez utiliser Pyzmq : https://zeromq.org/languages/python/

Une fois la bibliothèque installée vous pouvez exécuter les lignes suivantes afin de vérifier que tout fonctionne.

Commençons par nous connecter au serveur.
```python
import zmq

token = "gagner"
IP = "192.168.137.49"
port = 5555
context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":"+str(port))
```

Pour simplifier on donne deux fonctions d'écriture et de lecture du socket ZMQ.
```python
def write(content):
    socket.send(content.encode('UTF-8'))
def read():
    return socket.recv().decode('UTF-8')
```

Ensuite on envoie notre token au serveur et on récupère les informations sur la partie.
```python
# Envoi du token
write("token "+token)

# On récupère toutes les données
raw_data = read()
# Puis on les traite
data_lines = raw_data.split('\n')
# A vous de jouer !
# ...

# Enfin on dit qu'on est prêt pour commencer la partie
write("ok")
```

Un tour de la partie se décompose comme suit :
```python
# On récupère les données fournies par le serveur pour ce tour
raw_data = read()

# On les traite
data_lines = raw_data.split('\n')
# ...
```

Ensuite on peut réaliser des actions :
```python
# Se déplacer en marchant vers la cellule au dessus à droite
write("M 1\nEOI")

# Se déplacer en courant vers la cellule à gauche puis en haut à droite
write("MF 5 1\nEOI")

# Utiliser l'action courante
write("P\nEOI")

# Se débarasser de l'action courante et se déplacer à gauche
write("S\nM 5\nEOI")
```
