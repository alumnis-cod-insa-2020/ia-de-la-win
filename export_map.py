#!/usr/bin/env python3

import sys
import random

from pprint import pprint

import zmq
import sys


from action import *
from parsing import *
from util import *
from algo import *

# Dirty fix for Ctrl-C not working when ZMQ is waiting for msg
import signal

def kill_zmq_handler(signum, frame):
    sys.exit()

signal.signal(signal.SIGINT, kill_zmq_handler)

token = chr(0o141) # Nics

IP = "deepdev.ddns.net"
port = "557" + sys.argv[1]
context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":"+port)

def write(content):
    socket.send(content.encode('UTF-8'))
def read():
    return socket.recv().decode('UTF-8')


def play():
    write("token "+token)
    print('Token sent.')
    mapdata = read()
    print(len(mapdata))
    with open('grid_smart2.txt', 'w') as fo:
        fo.write(mapdata)
    
    write("ok")
    


def main():
    play()


if __name__ == '__main__':
    main()