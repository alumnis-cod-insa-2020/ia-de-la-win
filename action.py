def move(*pos):
    assert 1 <= len(pos) <= 2
    return f"M{'F' if len(pos)==2 else ''} {' '.join(map(str, pos))}"


# 0 -> open, 1-> close doors
def power(power_name, action=""):
    return "P" + (f" {action}" if action else "")


def drop_power():
    return "S"


def do_actions(*actions):
    return "\n".join(actions) + "\nEOI"