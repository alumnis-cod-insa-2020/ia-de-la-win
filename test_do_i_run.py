import parsing
import algo
from util import do_i_run


def test_check_not_far():
    "Track: R G G"
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 373, 374)
        assert do_i_run(track[1:], state, 2) is True
        assert do_i_run(track[1:], state, 1) is True
        assert do_i_run(track[1:], state, 0) is False


def test_check_not_far2():
    "Track: R G G"
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 373, 375)
        assert do_i_run(track[1:], state, 2) is True
        assert do_i_run(track[1:], state, 1) is True
        assert do_i_run(track[1:], state, 0) is False


def test_check_one_tar():
    "Track: R G G"
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 373, 380)
        assert do_i_run(track[1:], state, 2) is True
        assert do_i_run(track[1:], state, 1) is True
        assert do_i_run(track[1:], state, 0) is False


def test_check_two_tar():
    "Track: R R G"
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 372, 380)
        assert do_i_run(track[1:], state, 1) is False
        assert do_i_run(track[1:], state, 2) is True
        assert do_i_run(track[1:], state, 0) is False


def test_check_grass():
    "Track: G G G"
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        cost, track = algo.dijkstra(state, 245, 251)
        assert do_i_run(track[1:], state, 1) is False
        assert do_i_run(track[1:], state, 2) is False
        assert do_i_run(track[1:], state, 0) is False
