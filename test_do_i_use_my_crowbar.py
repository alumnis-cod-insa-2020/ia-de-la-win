import parsing
import algo
from util import *


def test_not_good_power():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        state.ourPower = POWER_VISION
        tile_info, traversable = state.grid[384]
        state.grid[384] = (traversable, False)
        assert do_i_use_my_crowbar(state, 384) is False


def test_no_power():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        state.ourPower = POWER_NONE
        tile_info, traversable = state.grid[384]
        state.grid[384] = (traversable, False)
        assert do_i_use_my_crowbar(state, 384) is False


def test_door_open():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        state.ourPower = POWER_CROWBAR
        assert do_i_use_my_crowbar(state, 384) is False


def test_door_closed():
    with open("./maps/grid_simp.txt", 'r') as f:
        state = parsing.parse_input_init(f.read())
        state.ourPower = POWER_CROWBAR
        tile_info, traversable = state.grid[384]
        state.grid[384] = (traversable, False)
        assert do_i_use_my_crowbar(state, 384) is True
