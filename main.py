#!/usr/bin/env python3/*
import logging
import random

import zmq
from time import time

from action import *
from parsing import *
from algo import *
from utilz import *

import banner  # noqa # pylint: disable=unused-import

# Dirty fix for Ctrl-C not working when ZMQ is waiting for msg
import signal


def kill_zmq_handler(signum, frame):
    sys.exit()


signal.signal(signal.SIGINT, kill_zmq_handler)

token = chr(0o141)  # Nics

# IP = "deepdev.ddns.net"
# port = "557" + sys.argv[1]

IP = "localhost"

port = "5576"  #! MODO C'est le bon port ???

context = zmq.Context()

socket = context.socket(zmq.REQ)
socket.connect("tcp://" + IP + (":" + port) if port else "")


def write(content):
    socket.send(content.encode('UTF-8'))


def read():
    return socket.recv().decode('UTF-8')


class Timer(object):
    def __init__(self, description):
        self.description = description

    def __enter__(self):
        self.start = time()

    def __exit__(self, type, value, traceback):
        self.end = time()
        print(f"{self.description}: {self.end - self.start}")

def wall_strat(s, wall_buzzer, next_buzzer):
    def goto(pos, wall=False):
        moves = []
        power_moves = [power(POWER_WALL)] if wall else []
        if wall:
            s.grid[s.ourPos] = TileInfo("B", False)
        dist, path = dijkstra(s, s.ourPos, pos)
        return path, power_moves

    if s.ourPower != POWER_WALL:
        print("Get Wall Power")
        return goto(wall_buzzer)

    # We have the WALLPOWER
    else:
        if s.ourPos in [k[0] for k in get_accessible_neighbours(wall_buzzer, s).values()]:
            print("USE WALL POWER")

            old_cell = s.grid[s.ourPos]
            s.grid[s.ourPos] = TileInfo("B", False)
            if distance(s, wall_buzzer, next_buzzer) != -1:
                print("Get wall power")
                target = wall_buzzer
            else:
                print("Next buzzer")
                target = next_buzzer
            
            s.grid[s.ourPos] = old_cell
            return goto(target, wall=True)
        
        nei = [k[0] for k in get_accessible_neighbours(wall_buzzer, s).values()]
        random.shuffle(nei)
        for cell in nei:
            if distance(s, cell, next_buzzer) != -1:
                print(f"Wall cell {cell}")
                return goto(cell)

def play():
    write("token " + token)
    print('Token sent.')

    s = parse_input_init(read())
    pprint(s)

    with Timer("Init turn"):
        first_turn = True
        stamina = 2

        dijsktra_cache = precompute_buzzers_distances(s)
        
        write("ok")

    door_left = 2

    turn = 0
    while True:

        with Timer(f"Server response time"):
            s = parse_input_turn(s, read())

        turn += 1
        with Timer(f"Turn {turn}"):

            if first_turn:
                mat_dist, buzz_order = calc_initial_paths_dists(s, dijsktra_cache=dijsktra_cache)
                buzz_iter = iter(buzz_order)
                tgt_buzz = next(buzz_iter)

                first_turn = False

            # Manage power trivia
            power_moves = []
            if s.ourPower == POWER_INVISIBILITY or s.ourPower == POWER_VISION:
                print("USE POWER !!")
                power_moves.append(power(s.ourPower))

            if s.ourPower == POWER_DOOR and door_left == 2: 
                door_left -= 1
                print("USE DOOR !!")
                power_moves.append(power(s.ourPower, action="0"))

            if s.ourPos == tgt_buzz:
                tgt_buzz = next(buzz_iter)
            # pprint(mat_dist)

            dist, path = dijkstra(s, s.ourPos, tgt_buzz)
            if any(opp.id == 'G' for opp in s.visibleOpponents):
                dist2, path2 = dijkstra(s, s.ourPos, tgt_buzz, False)
                if dist2 + 3 < dist:
                    actions = do_actions()
                    continue

            ''' TODO essayer de changer de cible
            if dist == -1:
                buzz_iter, buzz_l = it.tee(buzz_iter)
                buzz_l = list(buzz_l)
                for buzz_id in buzz_l:
                    dist, path = dijkstra(s, s.ourPos, buzz_id)
                    if dist != -1:
                        i = buzz_l.index(buzz_id)
                        buzz_iter = iter(buzz_l[i:] + buzz_l[:i] + [tgt_buzz])
                        break
            '''

            # TODO si ça marche pas, essayer de considérer les portes ouvertes

            # TODO si ça marche pas, random

            pprint(("beg loop", s, tgt_buzz, path[:10]))

            first_move = None
            second_move = None
            # Wall buzzer is blocked
            if distance(s, s.ourPos, buzz_order[0]) == -1:
                try:
                    neighbours = get_neighbours(s.ourPos, s)
                    first_move = next(i for i, (k, _) in neighbours.items() if k == path[1])

                    if len(path) > 2:
                        neighbours = get_neighbours(path[1], s)
                        second_move = next(i for i, (k, _) in neighbours.items() if k == path[2])
                    else:
                        second_move = random.choice([1, 2, 3, 4, 5, 6])
                except:
                    logging.exception("/!\ ERROR: please change the game and do not use bokeh please")
                    second_move = random.choice([1, 2, 3, 4, 5, 6])
            else:
                
                try:
                    path, power_moves = wall_strat(s, buzz_order[0], buzz_order[1])
                    pprint(("Walling", path[:10], s.ourPower))
                    neighbours = get_neighbours(s.ourPos, s)
                    first_move = next(i for i, (k, _) in neighbours.items() if k == path[1])
                    if len(path) > 2:
                        neighbours = get_neighbours(path[1], s)
                        second_move = next(i for i, (k, _) in neighbours.items() if k == path[2])
                    else:
                        second_move = random.choice([1, 2, 3, 4, 5, 6])
                except:
                    logging.exception("/!\ WALL STRAT CRASH")
                    first_move = random.choice([1, 2, 3, 4, 5, 6])

            try:
                if s.ourPower == POWER_DOOR and not s.grid[first_move][1] and door_left > 0: 
                    door_left -= 1
                    power_moves.append(power(s.ourPower, action="1"))
            except:
                logging.exception("/!\ DOOR EXCEPTION: unable to understand how a door works")

            try:
                if do_i_use_my_crowbar(s, first_move):
                    logging.info('CROWBAR USAGE')
                    power_moves.append(power(s.ourPower))
            except:
                logging.exception("/!\ CROWBAR EXCEPTION: please reboot the player")

            # while True:
            #     try:
            #         selected_action = input('New move : ')
            #         break
            #     except:
            #         print('Err, start again')
            #         pass

            moves = [first_move]
            if len(path) > 0 and do_i_run(path[1:], s, stamina):
                moves.append(second_move)
                stamina = max(0, stamina - 1)
            else:
                stamina = min(2, stamina + 1)

            actions = do_actions(
                *power_moves,
                move(*moves)
            )

            print(
                *power_moves,
                move(*moves))
            write(actions)


def main():
    play()


if __name__ == '__main__':
    main()
