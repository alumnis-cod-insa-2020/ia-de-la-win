# Wall Strategy

When quitting a buzzer:
 * Check at escape that if the path can be blocked (adjacent cell)
 * If at least two cell are adjacent, check distance between adjacent cells
 * If distance increase drastically with the wall
    * Check Traveler Salesman will not go in this cell  
    * If no, place wall 
