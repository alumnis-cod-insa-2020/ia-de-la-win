import sys
from pprint import pprint

from collections import defaultdict, namedtuple

from utilz import *


def parse_input_init(msg: str):
    lines = msg.split('\n')
    assert len(lines) > 2, f"Pas assez de lignes dans input init : {lines}"
    assert lines[0] == 'INIT', f"Première ligne d'init n'est pas INIT : {lines[0]}"
    colCount, lineCount = map(int, lines[1].split())
    grid = defaultdict(lambda: TileInfo("T", False))
    lineIndex = 2
    for i in range(lineCount):
        tiles = lines[lineIndex].strip().split()
        lineIndex += 1
        for j in range(colCount):
            traversable = tiles[j] not in ['W', 'T', 'L', 'B']
            grid[i * colCount + j] = TileInfo(tiles[j], traversable)

    #playerCount, ourId = map(int, lines[lineIndex].split())
    #lineIndex += 1
    # TODO read player info
    return State(grid=grid, colCount=colCount, lineCount=lineCount)


def parse_input_turn(state: State, msg: str):
    if msg in ['WON', 'KILLED', 'END']:
        sys.exit()
    lines = msg.split('\n')
    assert len(lines) > 3, "Pas assez de lignes dans input update"
    assert lines[0].startswith('TURN '), "Première ligne d'update ne commence pas par TURN"
    pos, tileType = lines[1].split()
    state.ourPos = int(pos)
    state.grid[state.ourPos] = TileInfo(tileType, state.grid[state.ourPos].traversable)
    state.ourPower = int(lines[2])
    state.ourSuspectedStatus = int(lines[3])
    lineIndex = 4
    if tileType.isdigit():
        #doorStatus = int(lines[lineIndex].split()[1])
        lineIndex += 1
    visibleCellCount = int(lines[lineIndex])
    lineIndex += 1
    for _ in range(visibleCellCount):
        cellId, cellType, canGo = lines[lineIndex].split()
        if state.grid[int(cellId)].type != "B": 
            state.grid[int(cellId)] = TileInfo(cellType, False if cellType == "B" else (canGo.strip() == '1'))
        lineIndex += 1
    visibleOpponents = int(lines[lineIndex])
    lineIndex += 1
    state.visibleOpponents = []
    for _ in range(visibleOpponents):
        _id, pos = lines[lineIndex].split()
        state.visibleOpponents.append(OpponentInfo(_id, int(pos)))
        if _id == "D":
            pprint((pos, state.grid[pos]))
        lineIndex += 1
    assert lines[lineIndex] == 'EOT', "Input update ne finit pas par EOT"
    return state




